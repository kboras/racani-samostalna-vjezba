import numpy
import math
from pyglet.gl import *

window = pyglet.window.Window(resizable=True)
window.maximize()

starting_vertices = []
polygons = []
vertices = []

current_spline_point = []
current_spline_idx = 0

original_orientation = []
current_orientation = []


# loadings

def load_trajectory():
    trajectory_ = open("trajectories/spiral", "r")
    lines = trajectory_.readlines()
    trajectory_new = []
    for line in lines:
        line = line.strip().split()
        x, y, z = float(line[0]), float(line[1]), float(line[2])
        trajectory_new.append((x, y, z))
    trajectory_new = numpy.array(trajectory_new)
    return trajectory_new


def load_object():
    global polygons, vertices, original_orientation, current_orientation
    object_ = open("objects/plane.obj", "r")
    lines = object_.readlines()
    for line in lines:
        line = line.strip()
        if len(line) == 0 or line[0] in ("#", "g"):
            continue
        line = line.split(" ")
        if line[0] == "v":
            vertex = list(map(float, line[1:]))
            vertices += [vertex]
        elif line[0] == "f":
            polygon = list(map(int, line[1:]))
            polygons += [polygon]

    scaled_vertices = []
    for v in vertices:
        nv = numpy.array(v)/5
        scaled_vertices.append(nv)
    vertices = scaled_vertices

    return vertices, polygons


# calculations

def get_angle_between_vectors(v1, v2):
    unit_v1 = v1 / numpy.linalg.norm(v1)
    unit_v2 = v2 / numpy.linalg.norm(v2)
    dot_product = numpy.dot(unit_v1, unit_v2)
    angle = numpy.arccos(dot_product)
    return angle


def construct_b_spline(trajectory_):
    spline_trajectory = []
    spline_derivations = []
    spline_normals = []
    spline_binormals = []

    n_of_segments = len(trajectory_) - 3

    mat = numpy.array([[-1, 3, -3, 1],
                       [3, -6, 3, 0],
                       [-3, 0, 3, 0],
                       [1, 4, 1, 0]])

    mat_der = numpy.array([[-1, 3, -3, 1],
                           [2, -4, 2, 0],
                           [-1, 0, 1, 0]])

    mat_second_der = numpy.array([[-1, 3, -3, 1],
                                  [1, -2, 1, 0]])

    segments = []
    segment_derivations = []
    segment_second_ders = []

    for i in range(1, n_of_segments + 1):
        point_batch = [trajectory_[i - 1], trajectory_[i], trajectory_[i + 1], trajectory_[i + 2]]
        segment = 1 / 6 * numpy.dot(mat, numpy.array(point_batch))
        segments.append(segment)
        segment_derivation = 1 / 2 * numpy.dot(mat_der, numpy.array(point_batch))
        segment_derivations.append(segment_derivation)
        segment_second_der = numpy.dot(mat_second_der, numpy.array(point_batch))
        segment_second_ders.append(segment_second_der)

    for segment, derivation, second_der in zip(segments, segment_derivations, segment_second_ders):
        for t in numpy.arange(0, 1, 0.01):
            point = numpy.dot(numpy.array([t ** 3, t ** 2, t, 1]), segment)
            point_der = numpy.dot(numpy.array([t ** 2, t, 1]), derivation)
            point_second_der = numpy.dot(numpy.array([t, 1]), second_der)
            point_norm = numpy.cross(point_der, point_second_der)
            spline_trajectory.append(point)
            spline_derivations.append(point_der)
            spline_normals.append(point_norm)
            point_binorm = numpy.cross(point_der, point_norm)
            spline_binormals.append(point_binorm)

    return spline_trajectory, spline_derivations, spline_normals, spline_binormals


def set_beginning_orientation():
    global vertices, starting_vertices
    new_vertices = []

    centroid = numpy.array(vertices[0])
    for vertex in vertices[1:]:
        centroid += vertex
    centroid /= len(vertices)
    orientation_vector = vertices[0] - centroid
    orientation_vector_h = [orientation_vector[0], orientation_vector[1], orientation_vector[2], 0]

    target_orientation = numpy.array(derivations[0])

    yz_orientation_projection = numpy.array([0, orientation_vector[1], orientation_vector[2]])
    yz_angle = get_angle_between_vectors(yz_orientation_projection, target_orientation)
    R_yz = numpy.array([[1, 0, 0, 0],
                        [0, math.cos(yz_angle), math.sin(yz_angle), 0],
                        [0, -math.sin(yz_angle), math.cos(yz_angle), 0],
                        [0, 0, 0, 1]])
    new_orientation_vector = numpy.dot(R_yz, orientation_vector_h)

    xz_orientation_projection = numpy.array([new_orientation_vector[0], 0, new_orientation_vector[2]])
    xz_angle = get_angle_between_vectors(xz_orientation_projection, target_orientation)
    R_xz = numpy.array([[math.cos(xz_angle), 0, math.sin(xz_angle), 0],
                        [0, 1, 0, 0],
                        [-math.sin(xz_angle), 0, math.cos(xz_angle), 0],
                        [0, 0, 0, 1]])

    for vertex in starting_vertices:
        diff = numpy.array(spline[0]) - starting_vertices[0]
        new_vertex = vertex + diff
        new_vertex = numpy.array([new_vertex[0], new_vertex[1], new_vertex[2], 0])
        new_vertex = numpy.dot(R_yz, new_vertex)
        new_vertex = numpy.dot(R_xz, new_vertex)
        new_vertex = new_vertex[:-1]
        new_vertices.append(new_vertex)

    vertices = new_vertices
    starting_vertices = new_vertices


def translate_and_rotate_object():
    global polygons, vertices

    new_vertices = []
    for vertex in starting_vertices:
        diff = numpy.array([0, 0, 0]) - starting_vertices[0]
        new_vertex = vertex + diff

        w = derivations[current_spline_idx]
        w /= numpy.linalg.norm(w)
        u = normals[current_spline_idx]
        u /= numpy.linalg.norm(u)
        v = binormals[current_spline_idx]
        v /= numpy.linalg.norm(v)
        R = numpy.transpose(numpy.array([w, u, v]))
        R_inv = numpy.linalg.inv(R)
        new_vertex = numpy.dot(R_inv, new_vertex)

        new_vertex -= diff

        diff = current_spline_point - starting_vertices[0]
        new_vertex += diff
        new_vertices.append(new_vertex)
    vertices = new_vertices


# drawing

def draw_derivation():
    spline_point = current_spline_point
    try:
        derivation = derivations[current_spline_idx]
    except IndexError:
        derivation = derivations[-1]
    normalized_derivation_line = numpy.array(derivation) / numpy.linalg.norm(derivation)
    normalized_derivation_line += spline_point
    glBegin(GL_LINE_STRIP)
    glColor3f(0.0, 0.0, 1.0)
    glVertex3f(spline_point[0], spline_point[1], spline_point[2])
    glVertex3f(normalized_derivation_line[0], normalized_derivation_line[1], normalized_derivation_line[2])
    glEnd()


def draw_b_spline():
    glBegin(GL_LINE_STRIP)
    for point in spline:
        glVertex3f(point[0], point[1], point[2])
        glColor3f(1.0, 1.0, 1.0)
    glEnd()


def draw_spline_polygon():
    glBegin(GL_LINE_STRIP)
    for point in trajectory:
        glColor3f(1.0, 0.0, 0.0)
        glVertex3f(point[0], point[1], point[2])
    glEnd()


def draw_object_and_derivation():
    global current_spline_point, current_spline_idx
    translate_and_rotate_object()
    draw_derivation()
    for polygon in polygons:
        glBegin(GL_POLYGON)
        for idx in polygon:
            vertex = vertices[idx - 1]
            glColor3f(0.0, 1.0, 0.0)
            glVertex3f(vertex[0], vertex[1], vertex[2])
        glEnd()
    current_spline_idx += 1
    try:
        current_spline_point = spline[current_spline_idx]
    except IndexError:
        current_spline_point = spline[-1]
        current_spline_idx = 0


@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT)
    draw_b_spline()
    draw_spline_polygon()
    draw_object_and_derivation()


@window.event
def on_resize(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60, float(width) / float(height), 0.1, 256)
    glTranslated(0, 3.0, -8.0)
    glRotated(60, 1, 0, 0)
    return True


def update(*args):
    pass


if __name__ == "__main__":
    trajectory = load_trajectory()
    spline, derivations, normals, binormals = construct_b_spline(trajectory)
    vertices, polygons = load_object()
    starting_vertices = vertices.copy()
    set_beginning_orientation()

    current_spline_idx = 0
    current_spline_point = spline[0]

    pyglet.clock.schedule(update, .5)
    pyglet.app.run()
