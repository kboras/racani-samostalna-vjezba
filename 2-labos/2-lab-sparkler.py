import numpy
from pyglet.gl import *
from random import randrange
import pygame
from PIL import Image

window = pyglet.window.Window()
window.maximize()
window.set_mouse_visible(False)

particles = []
num_of_particles = 1000


def get_random_color():
    r = randrange(40, 100, 1) / 100
    g = randrange(40, 100, 1) / 100
    b = randrange(40, 100, 1) / 100
    return r, g, b


class Particle:

    def __init__(self):
        global mouse_x, mouse_y
        self.t = 0
        r, g, b = get_random_color()
        self.color = [r, g, b]
        self.position = [mouse_x, mouse_y, 0]
        self.size = 2
        self.ttl = randrange(50, 100, 1)
        self.wait = 0
        self.range = randrange(50, 100, 1) / 100
        self.final_position = [randrange(-500, 500, 1) * self.range, randrange(-100, 700, 1) * self.range, 0]

    def __getitem__(self, item):
        return self.position[item]

    def live(self):
        self.t += 1
        self.change_color()
        self.change_position()
        self.change_size()
        if self.t >= self.ttl:
            self.die_and_be_reborn()

    def change_color(self):
        r, g, b = self.color[0], self.color[1], self.color[2]
        percent = self.t / self.ttl * 0.02
        rgb = [r - percent, g - percent, b - percent]
        self.color = rgb

    def change_position(self):
        if self.t < self.wait:
            return
        delta_x = self.final_position[0] * 0.01
        delta_y = self.final_position[1] * 0.01
        delta_z = 0
        gravity = self.t * -0.09
        x, y, z = self.position[0] + delta_x, self.position[1] + delta_y + gravity, self.position[2] + delta_z
        self.position = [x, y, z]

    def change_size(self):
        percent = (self.t ** 2) / self.ttl * 0.001
        self.size = self.size + percent

    def die_and_be_reborn(self):
        global mouse_x, mouse_y
        self.t = 0
        r, g, b = get_random_color()
        self.color = [r, g, b]
        self.position = [mouse_x, mouse_y, 0]
        self.size = 2
        self.ttl = randrange(50, 100, 1)
        self.wait = 0
        self.final_position = [randrange(-500, 500, 1) * self.range, randrange(-100, 700, 1) * self.range, 0]


def generate_particles():
    for _ in range(num_of_particles):
        p = Particle()
        particles.append(p)


def circle_of_life():
    global particles
    for particle in particles:
        particle.live()


def draw_stick():
    global mouse_x, mouse_y
    glBegin(GL_LINE_STRIP)
    glColor3f(204, 51, 0)
    glVertex3f(mouse_x, mouse_y, 0)
    glColor3f(204, 51, 0)
    glVertex3f(100 + mouse_x, -200 + mouse_y, 0)
    glEnd()


def draw_particle(particle):
    x, y, z = particle.position[0], particle.position[1], particle.position[2]
    glPointSize(particle.size)
    glBegin(GL_POINTS)
    glColor3f(particle.color[0], particle.color[1], particle.color[2])
    glVertex3f(x, y, z)
    glEnd()


def draw_particles():
    for particle in particles:
        draw_particle(particle)


@window.event
def on_draw():
    glClear(GL_COLOR_BUFFER_BIT)
    draw_particles()
    draw_stick()
    circle_of_life()


@window.event
def on_mouse_motion(x, y, dx, dy):
    global mouse_x, mouse_y
    mouse_x = x - 60
    mouse_y = y + 120


def update(*args):
    pass


mouse_x, mouse_y = -1000, -1000

if __name__ == "__main__":
    generate_particles()

    pyglet.clock.schedule(update, .5)
    pyglet.app.run()
